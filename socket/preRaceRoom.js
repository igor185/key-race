const raceRoom = require('./raceRoom');
const maxAmountOfUsers = 5;

let existPreRaceTimer = false;

const timeToRace = (socket, usersWait, default_time) => {
    let left_time = default_time;
    return setInterval(() => {
        if (usersWait.usersWait.size === 0) {
            left_time = default_time;
            return;
        }
        if (left_time <= 2000) {
            if (usersWait.usersWait.size >= 1) {
                beginRace(socket, usersWait);
                return;
            }
            left_time = default_time + 1000;
        }
        left_time -= 1000;
        socket.emit('time_preRace', {time: left_time, message: "Race in will begin when there are 5 users here or through "});
        socket.broadcast.emit('time_preRace', {time: left_time, message: "Race in will begin when there are 5 users here or through "});
    }, 1000);
};

const beginRace = (socket, usersWait) => {
    const raceNumber = Math.round(Math.random() * 100000 + 1000);
    const users = [...usersWait.usersWait];
    usersWait.usersWait.clear();

    socket.emit('nearlyStart', {raceNumber, users});
    socket.broadcast.emit('nearlyStart', {raceNumber, users});
};

module.exports = (socket, user, usersWait) => {
    let timer;
    if (!existPreRaceTimer) {
        timer = timeToRace(socket.to('waitRoom'), usersWait,30000);
        existPreRaceTimer = true;
    }
    if (usersWait.usersWait.size === maxAmountOfUsers)
        beginRace(socket.to('waitRoom'), usersWait);

    socket.on('join_race', ({room, amountUsers}) => {
        clearInterval(timer);
        socket.leave('waitRoom');
        existPreRaceTimer=false;
        socket.join(room);
        socket.emit('join_race');
        raceRoom(socket, room, user, amountUsers);
    });

    socket.on('disconnect', () => {
        usersWait.usersWait.delete(user.login);
        socket.broadcast.emit('leaveUser', {user: user.login});
    });

};
