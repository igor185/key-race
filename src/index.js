const io = require('socket.io-client');
import preRace from './component/preRace'

class App {
    constructor() {
        const token = localStorage.getItem('jwt');

        const remove_jwt = () => {
            localStorage.setItem('jwt','');
            location.replace('/');
        };

        if (token) {
            const socket = io({
                query: {token}
            });
            socket.on('connect', () => {
                socket.on('delete_jwt',remove_jwt);
                new preRace(document.getElementById('preRace'), socket, token);
                socket.off('delete_jwt', remove_jwt);
            });
        } else {
            location.replace('/')
        }
    }
}

window.onload = ()=> {
    new App();
};