const send = (socket, path, params) => {
    socket.broadcast.emit(path, {...params});
    socket.emit(path, {...params});
};
let existRaceTimer = false;

const timeRace = (socket, default_time, raceNumber) => {
    let left_time = default_time;
    return setInterval(() => {
        if(left_time ===0)
            return;
        if(left_time <= 1000){
            socket.emit('finish', {time: left_time, raceNumber});
            socket.broadcast.emit('finish', {time: left_time, raceNumber});
            send(socket, 'end_time', {default_time:120000, raceNumber});
            send(socket, 'endRace', {raceNumber});
            left_time = 0;
            existRaceTimer = false;
            return;
        }
        left_time -= 1000;
        socket.emit('time_race', {time: left_time, message:'time to finish', default_time, raceNumber});
        socket.broadcast.emit('time_race', {time: left_time, message:'time to finish', default_time, raceNumber});
    }, 1000);
};

const timeToRaceStart = (socket, default_time, amountUsers, raceNumber) => {

    let left_time = default_time;
    return setInterval(() => {
        if(left_time === 0) {
            clearInterval(timers.get(raceNumber));
            timers.delete(raceNumber);
            return;
        }
        if(left_time <= 1000){
            socket.emit('start', {time: left_time,raceNumber});
            socket.broadcast.emit('start', {time: left_time,raceNumber});
            left_time = 0;
            const timer = timeRace(socket, 60000, raceNumber);
            socket.on('userEndRace',(data) =>{
                socket.emit('userEndRace', data);
                socket.broadcast.emit('userEndRace', data);
                amountUsers--;
                if(amountUsers === 0) {
                    send(socket, 'endRace', {raceNumber});
                    clearInterval(timer);
                }
            });
            return;
        }
        left_time -= 1000;
        socket.emit('time_preStart', {time: left_time, message:'time to start', raceNumber});
        socket.broadcast.emit('time_preStart', {time: left_time, message:'time to start', raceNumber});
    }, 1000);
};

const timers = new Map();

module.exports = (socket, raceNumber, user, amountUsers) => {

    if(!timers.has(raceNumber))
        timers.set(raceNumber, timeToRaceStart(socket.to(raceNumber), 30000, amountUsers, raceNumber));

    socket.on('progress', (data) => {
        send(socket.to(data.raceNumber), 'progress', {...data, user: user.login});
    });
    socket.on('disconnect', () => {
        socket.to(raceNumber).broadcast.emit('leaveUser', {username: user.login});
    });
};
