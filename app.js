const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

const logger = require('morgan');
const path = require('path');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');

const users = require('./users');
const texts = require('./texts');

const secret = "secretWord";
require("./config/passport.config.js");
app.use(express.static(path.join(__dirname, 'dist')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));


app.use(passport.initialize());


const setHandlers = require('./socket/index');

const usersWait = {usersWait : new Set()};

io.on('connection', (socket) => {
    setHandlers(socket, usersWait);
});

app.get('/', (req, res) => {
    res.sendFile(__dirname + "/dist/login.html");
});

app.get('/usersAwait',passport.authenticate('jwt', {session: false}), (req,res) => {
    res.json([...usersWait.usersWait]);
});

app.post('/text',passport.authenticate('jwt', {session: false}),(req,res) => {
    if(!req.body.race)
        res.status(401).send();

    console.log(texts.length);
    res.json(texts[req.body.race%texts.length]);
});

app.post("/name", (req, res) => {
    console.log(req.body);
    res.status(200).send();
});
app.use('/login', (req, res) => {
    const userReq = req.body;
    const userDB = users.find(user => user.login === userReq.login);

    if (userDB && userDB.password === userReq.password) {
        const token = jwt.sign(userReq, secret, {});
        res.status(200).json({auth: true, token});
    } else
        res.status(401).json({auth: false});
});

app.use('/game'/* password.authenticate('jwt')*/, (req, res) => {
    res.sendFile(__dirname + "/dist/game.html");
});

http.listen(3000, () => {
    console.log("Start on port 3000!")
});
